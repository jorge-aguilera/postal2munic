def publicFile = new File('build')
if( publicFile.exists() ){
  publicFile.eachDir(){ dir ->
    dir.deleteDir()
  }
}else{
  publicFile.mkdirs()
}
def postalFile = new File('postal',publicFile)
def muniFile = new File('provincia',publicFile)
if(  !postalFile.mkdirs() || !muniFile.mkdirs() ){
  println "no pueddoorrr"
  //return -1
}
new File("FicheroRelacion_INE_CodigoPostal.txt").eachLine(1){ line, idx->
  // cabecera no hace falta
  if( idx == 1) return
  def fields = line.split(';')

  new File("${fields[2]}.json",postalFile).withWriter{
    it.writeLine "{'provincia':'${fields[0]}','municipio':'${fields[1]}','nombre':'${fields[3]}'}"
  }

  def provincia = new File(fields[0], muniFile)
  provincia.mkdirs()
  new File("${fields[1]}.json",provincia).withWriter{
    it.writeLine "{'codpostal':'${fields[2]}','nombre':'${fields[3]}',}"
  }

}
