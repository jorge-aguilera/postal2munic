def jsFile = new File('postal2munic.js', new File('build') ).withWriter{ writer->
  writer.writeLine "var postal2munic = {};"
  new File("FicheroRelacion_INE_CodigoPostal.txt").eachLine(1){ line, idx->
    // cabecera no hace falta
    if( idx == 1) return
    def fields = line.split(';')
    writer.writeLine "postal2munic['${fields[2]}']={'provincia':'${fields[0]}','municipio':'${fields[1]}','nombre':'${fields[3].replaceAll('\'',' ')}'}"
  }
}
