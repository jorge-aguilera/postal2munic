# Postal2Munic

## Codigos Postales a Municipio (y viceversa)


Servicio REST para consulta de códigos postales de España a municipios

*https://jorge-aguilera.gitlab.io/postal2munic/postal/XXXX.json*

donde XXXX es un Código Postal (i.e 28010), y obtendrás la provincia, municipio y nombre
correspondiente (en formato json)

*https://jorge-aguilera.gitlab.io/post2munic/provincia/XX/YYY.json*

donde XX es el código de provincia e YYY el código de municipio y obtendrás el código
posta y el nombre del municipio (en formato json)

## Groovy + Gitlab + Docker + Asciidoctor

*https://jorge-aguilera.gitlab.io/post2munic*

